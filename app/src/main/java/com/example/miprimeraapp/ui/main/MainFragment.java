package com.example.miprimeraapp.ui.main;

import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.miprimeraapp.MainActivity2;
import com.example.miprimeraapp.R;

import java.util.concurrent.atomic.AtomicInteger;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        Button btnNavegar = view.findViewById(R.id.btnNavega);

        Button btnSegundaPantalla = view.findViewById(R.id.btnSegundaPantalla);

        TextView message = view.findViewById(R.id.mensaje);

        AtomicInteger veces = new AtomicInteger();

        btnNavegar.setOnClickListener(v -> {
            veces.getAndIncrement();
            message.setText("Me has hecho click " + veces + " !!");
        });

        btnSegundaPantalla.setOnClickListener(v ->{
            Intent i = new Intent(getContext(), MainActivity2.class);
            startActivity(i);
        });



        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}